import 'package:flutter/material.dart';
import 'package:foodate/common/common.dart';
import 'package:foodate/appbars/foodate_appbar.dart';
import 'package:foodate/appbars/foodate_bottom_appbar.dart';
import 'package:foodate/navigation/navigation_page.dart';
import 'package:foodate/pages/profile_page.dart';
import 'package:foodate/pages/explore_page.dart';
import 'package:foodate/pages/recipes_page.dart';

void main() => runApp(FooDate());

class FooDate extends StatefulWidget {
  @override
  _FooDateState createState() => _FooDateState();
}

class _FooDateState extends State<FooDate> {
  int currentPage = 0;
  List<NavigationPage> pages = [
    NavigationPage(
      title: 'Recipes',
      icon: Icons.library_books,
      page: RecipesPage(),
    ),
    NavigationPage(
      title: 'Stories',
      icon: Icons.add_circle_outline,
      page: ProfilePage(),
    ),
    NavigationPage(
      title: 'Explore',
      icon: Icons.explore,
      page: ExplorePage(),
    ),
    NavigationPage(
      title: 'Profile',
      icon: Icons.person,
      page: ProfilePage(),
    ),
    NavigationPage(
      title: 'Profile',
      icon: Icons.person,
      page: ProfilePage(),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light().copyWith(
        primaryColor: kPrimaryColor,
        primaryColorDark: kDarkPrimaryColor,
        primaryColorLight: kLightPrimaryColor,
        accentColor: kAccentColor,
        dividerColor: kDividerColor,
        primaryTextTheme: ThemeData.light().primaryTextTheme.apply(
              bodyColor: kTitleTextColor,
              displayColor: kTitleTextColor,
            ),
        textTheme: ThemeData.light().primaryTextTheme.apply(
              bodyColor: kBodyTextColor,
              displayColor: kBodyTextColor,
            ),
      ),
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Expanded(
                  flex: 9,
                  child: Scaffold(
                  appBar: FooDateAppBar(),
                )
              ),
              Expanded(
                flex: 100,
                child: pages[currentPage].page,
              ),
              Expanded(
                flex: 10,
                child: Scaffold(
                  bottomNavigationBar: FooDateBottomAppbar(
                    pages: pages,
                    currentIndex: currentPage,
                    onTap: (int index) {
                      setState(() {
                        currentPage = index;
                      });
                    },
                  ),
                )
              )
            ],
          ),
        ),
      ),
    );
  }
}
