import 'package:flutter/material.dart';

class NavigationPage {
  final Widget page;
  final IconData icon;
  final String title;

  NavigationPage({this.page, this.icon, this.title});
}
