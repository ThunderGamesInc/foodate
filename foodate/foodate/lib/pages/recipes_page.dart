import 'package:flappy_search_bar/search_bar_style.dart';
import 'package:flutter/material.dart';
import 'package:foodate/pages_resources/recipes_page/utils.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:foodate/common/common.dart';

class RecipesPage extends StatefulWidget {
  const RecipesPage({Key key}) : super(key: key);

  @override
  _RecipesPageState createState() => _RecipesPageState();
}

class _RecipesPageState extends State<RecipesPage> {
  List recipes;
  List matchedRecipes;
  String filter;

  @override
  void initState() {
    recipes = getRecipes();
    matchedRecipes = recipes;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ListTile makeListTile(Recipe recipe) => ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 15.0),
          leading: RecipeLeading(recipe),
          title: RecipeTitle(recipe),
          subtitle: RecipeSubtitle(recipe),
          trailing: RecipeTrailing(recipe),
//    onTap: () {
//        Navigator.push(
//            context,
//            MaterialPageRoute(
//                builder: (context) => DetailPage(lesson: lesson)));
//      },
        );

    dynamic makeTopRecipeCard(Recipe recipe, String filter, int index) {
      dynamic card = Container(
          padding: EdgeInsets.only(right: 15.0),
          child: GestureDetector(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 100.0,
                width: 300.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(const Radius.circular(10.0)),
                  image: DecorationImage(
                      image: NetworkImage(recipe.photo), fit: BoxFit.cover),
                ),
              ),
              Text(
                recipe.title,
                style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold),
              ),
              RatingBarIndicator(
                rating: recipe.rating,
                itemBuilder: (context, index) => Icon(
                  Icons.star,
                  color: kLightPrimaryColor,
                ),
                itemCount: 5,
                itemSize: 20.0,
              ),
            ],
          ),
        )
      );
      return filter == null || filter == "" ? card : Container(height: 0.0);
    }

    Card makeRecipeCard(Recipe recipe) {
      return Card(
        elevation: 0.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Container(
          child: makeListTile(recipe),
        ),
      );
    }

    void updateRecipes(String text) {
      List matched_recipes = [];

      if (text == "" || text == null) {
        matched_recipes = recipes;
      } else {
        for (Recipe recipe in recipes) {
          if (recipe.contains(text.toLowerCase())) {
            matched_recipes.add(recipe);
          }
        }
      }

      setState(() {
        filter = text;
        matchedRecipes = matched_recipes;
      });
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: Column(
          children: <Widget>[
            Container(
              height: 80.0,
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: SearchBar<Recipe>(
                minimumChars: 0,
                onSearch: (String text) {
                  updateRecipes(text);
                  return;
                },
                onCancelled: () {
                  updateRecipes(null);
                  return;
                },
                searchBarStyle: SearchBarStyle(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0),
                  borderRadius: BorderRadius.all(Radius.circular(50.0)),
                ),
              ),
            ),
            Expanded(
                child: CustomScrollView(
              slivers: [
                if (filter == null || filter == '')
                  SliverToBoxAdapter(
                    child: SizedBox(
                      height: 150.0,
                      child: ListView.builder(
                          itemExtent: 200,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) =>
                              makeTopRecipeCard(recipes[index], filter, index),
                          itemCount: 5),
                    ),
                  ),
                SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1,
                    childAspectRatio: 5,
                  ),
                  delegate: SliverChildListDelegate(
                    [for (Recipe recipe in matchedRecipes) makeRecipeCard(recipe)]
                  ),
                ),
              ],
            )),
          ],
        ),
      ),
    );
  }
}
