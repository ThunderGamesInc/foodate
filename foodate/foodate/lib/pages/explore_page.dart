import 'package:flutter/material.dart';

import 'package:foodate/pages_resources/explore_page/slide_food.dart';

class ExplorePage extends StatefulWidget {
  const ExplorePage({ Key key}) : super(key: key);

  @override
  _ExplorePageState createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          CardsSectionDraggable(),
          Expanded(flex:1, child: buttonsRow())
        ],
      ),
    );
  }
  Widget buttonsRow() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(flex: 1, child: Container()),
            Expanded(
              flex: 8,
              child: FloatingActionButton(
              onPressed: () {},
              backgroundColor: Colors.white,
              child: Icon(Icons.close, color: Colors.red),
          ),
            ),
          Expanded(
            flex: 8,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(flex: 1, child: Container()),
                Expanded(
                  flex: 20,
                  child: FloatingActionButton(
                    mini: true,
                    onPressed: () {},
                    backgroundColor: Colors.white,
                    child: Icon(Icons.star, color: Colors.blue),
                  ),
                ),
                Expanded(flex: 6, child: Container()),
              ],
            ),
          ),
          Expanded(
            flex: 8,
            child: FloatingActionButton(
              onPressed: () {},
              backgroundColor: Colors.white,
              child: Icon(Icons.favorite, color: Colors.green),
            ),
          ),
          Expanded(flex: 1, child: Container()),
        ],
      ),
    );
  }
}