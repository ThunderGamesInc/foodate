import 'package:flutter/material.dart';

final List<String> foodImages = [
  'https://images2.minutemediacdn.com/image/upload/c_crop,h_1126,w_2000,x_0,y_181/f_auto,q_auto,w_1100/v1554932288/shape/mentalfloss/12531-istock-637790866.jpg',
  'https://blogs.biomedcentral.com/on-medicine/wp-content/uploads/sites/6/2019/09/iStock-1131794876.t5d482e40.m800.xtDADj9SvTVFjzuNeGuNUUGY4tm5d6UGU5tkKM0s3iPk-620x342.jpg',
  'https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/publications/food-beverage-nutrition/foodnavigator.com/article/2017/12/06/why-sugar-and-why-so-much-who-investigates-the-food-industry-s-sweet-tooth/7624387-1-eng-GB/Why-sugar-and-why-so-much-WHO-investigates-the-food-industry-s-sweet-tooth_wrbm_large.jpg',
];

class CardsSectionDraggable extends StatefulWidget {
  @override
  _CardsSectionState createState() => _CardsSectionState();
}

class _CardsSectionState extends State<CardsSectionDraggable> {
  bool dragOverTarget = false;
  bool like = false;
  List<ProfileCardDraggable> cards = List();
  int cardsCounter = 0;

  @override
  void initState() {
    super.initState();

    for (cardsCounter = 0; cardsCounter < 3; cardsCounter++) {
      cards.add(ProfileCardDraggable(cardsCounter));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        flex: 5,
        child: Stack(
          children: <Widget>[
            // Drag target row
            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                dragTarget(),
                Flexible(flex: 2, child: Container()),
                dragTarget()
              ],
            ),
            // Back card
            Align(
              alignment: Alignment(0.0, -0.9),
              child: IgnorePointer(
                  child: SizedBox.fromSize(
                size: Size(MediaQuery.of(context).size.width * 0.8,
                    MediaQuery.of(context).size.height * 0.5),
                child: cards[2],
              )),
            ),
            // Middle card
            Align(
              alignment: Alignment(0.0, -0.7),
              child: IgnorePointer(
                  child: SizedBox.fromSize(
                size: Size(MediaQuery.of(context).size.width * 0.85,
                    MediaQuery.of(context).size.height * 0.55),
                child: cards[1],
              )),
            ),
            // Front card
            Align(
              alignment: Alignment(0.0, -0.0),
              child: Draggable(
                feedback: SizedBox.fromSize(
                  size: Size(MediaQuery.of(context).size.width * 0.9,
                      MediaQuery.of(context).size.height * 0.6),
                  child: cards[0],
                ),
                child: SizedBox.fromSize(
                  size: Size(MediaQuery.of(context).size.width * 0.9,
                      MediaQuery.of(context).size.height * 0.6),
                  child: cards[0],
                ),
                childWhenDragging: Container(),
              ),
            ),
          ],
        ));
  }

  void changeCardsOrder() {
    setState(() {
      if (foodImages.length <= cardsCounter + foodImages.length) {
        cardsCounter = 0;
      }
      cards[0] = cards[1];
      cards[1] = cards[2];
      cards[2] = ProfileCardDraggable(cardsCounter);
      cardsCounter++;
    });
  }

  Widget dragTarget() {
    return Flexible(
      flex: 1,
      child: DragTarget(
        builder: (_, __, ___) {
          return Container();
        },
        onWillAccept: (_) {
          setState(() => dragOverTarget = true);
          return true;
        },
        onAccept: (_) {
          print (like);
          changeCardsOrder();
          setState(() => dragOverTarget = false);
        },
        onLeave: (_) {
          setState(() => dragOverTarget = false);
        },
      ),
    );
  }
}

class ProfileCardDraggable extends StatelessWidget {
  final int cardNum;
  ProfileCardDraggable(this.cardNum);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 40,
              child: Image.network(foodImages[cardNum], fit: BoxFit.cover),
            ),
            Expanded(
              flex: 1,
              child: Container(),
            ),
            Expanded(
              flex: 10,
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('המבורגר מושחט',
                          style: TextStyle(
                              fontSize: 20.0, fontWeight: FontWeight.w700)),
                      Padding(padding: EdgeInsets.only(bottom: 8.0)),
                      Text('מסעדת פורט 19', textAlign: TextAlign.start),
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}
