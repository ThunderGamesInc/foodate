import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:foodate/common/common.dart';

class Recipe {
  String title;
  String category;
  double rating;
  String author;
  String photo;
  String authorPhoto;

  bool contains(String text) {
    return this.title.contains(text) || this.author.contains(text) || this.category.contains(text);
  }
  Recipe(
      {this.title, this.category, this.rating, this.author, this.photo, this.authorPhoto});
}


class RecipeTrailing extends StatelessWidget {
  final Recipe recipe;
  RecipeTrailing(this.recipe);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 20,
      backgroundImage: NetworkImage(recipe.authorPhoto),
    );
  }
}

class RecipeSubtitle extends StatelessWidget {
  final Recipe recipe;
  RecipeSubtitle(this.recipe);

  @override
  Widget build(BuildContext context) {
    return RatingBarIndicator(
      rating: recipe.rating,
      itemBuilder: (context, index) => Icon(
        Icons.star,
        color: kLightPrimaryColor,
      ),
      itemCount: 5,
      itemSize: 20.0,
    );
  }
}

class RecipeTitle extends StatelessWidget {
  final Recipe recipe;
  RecipeTitle(this.recipe);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            recipe.title,
            style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold),
          ),
          Text(
              recipe.category,
              style: TextStyle(fontSize: 12, color: Colors.black54, fontWeight: FontWeight.bold)
          ),
        ],
      ),
    );
  }
}

class RecipeLeading extends StatelessWidget {
  final Recipe recipe;
  RecipeLeading(this.recipe);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 12.0),
      height: 60.0,
      width: 80.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(const Radius.circular(10.0)),
        image: DecorationImage(
            image: NetworkImage(recipe.photo),
            fit: BoxFit.cover
        ),
      ),
    );
  }
}

List getRecipes() {
  return [
    Recipe(
      title: 'פצצת לוטוס',
      category: 'קינוחים',
      rating: 4.0,
      author: 'אור שפיץ',
      photo: 'https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      authorPhoto:'https://assets.website-files.com/5b9b9d35b759ef07a158b532/5b9ba578899c1843fece25f0_Image-19-03-2018-at-12.05-p-800.png',
    ),
    Recipe(
      title: 'פצצת לוטוס',
      category: 'בשר',
      rating: 4.0,
      author: 'אור שפיץ',
      photo: 'http://www.animalshop.co.il/images/itempics/uploads/media_23062019150926.jpg?rnd=.1262781?rnd=0.8749624067087183',
      authorPhoto:'https://assets.website-files.com/5b9b9d35b759ef07a158b532/5b9ba578899c1843fece25f0_Image-19-03-2018-at-12.05-p-800.png',
    ),
    Recipe(
      title: 'פצצת לוטוס',
      category: 'קינוחים',
      rating: 4.0,
      author: 'אור שפיץ',
      photo: 'http://www.animalshop.co.il/images/itempics/uploads/media_23062019150926.jpg?rnd=.1262781?rnd=0.8749624067087183',
      authorPhoto:'https://assets.website-files.com/5b9b9d35b759ef07a158b532/5b9ba578899c1843fece25f0_Image-19-03-2018-at-12.05-p-800.png',
    ),
    Recipe(
      title: 'פצצת לוטוס',
      category: 'קינוחים',
      rating: 4.0,
      author: 'אור שפיץ',
      photo: 'http://www.animalshop.co.il/images/itempics/uploads/media_23062019150926.jpg?rnd=.1262781?rnd=0.8749624067087183',
      authorPhoto:'https://assets.website-files.com/5b9b9d35b759ef07a158b532/5b9ba578899c1843fece25f0_Image-19-03-2018-at-12.05-p-800.png',
    ),
    Recipe(
      title: 'פצצת לוטוס',
      category: 'קינוחים',
      rating: 4.0,
      author: 'אור שפיץ',
      photo: 'http://www.animalshop.co.il/images/itempics/uploads/media_23062019150926.jpg?rnd=.1262781?rnd=0.8749624067087183',
      authorPhoto:'https://assets.website-files.com/5b9b9d35b759ef07a158b532/5b9ba578899c1843fece25f0_Image-19-03-2018-at-12.05-p-800.png',
    ),
    Recipe(
      title: 'פצצת לוטוס',
      category: 'קינוחים',
      rating: 4.0,
      author: 'אור שפיץ',
      photo: 'http://www.animalshop.co.il/images/itempics/uploads/media_23062019150926.jpg?rnd=.1262781?rnd=0.8749624067087183',
      authorPhoto:'https://assets.website-files.com/5b9b9d35b759ef07a158b532/5b9ba578899c1843fece25f0_Image-19-03-2018-at-12.05-p-800.png',
    ),
    Recipe(
      title: 'פצצת לוטוס',
      category: 'קינוחים',
      rating: 4.0,
      author: 'אדיר חזיז',
      photo: 'http://www.animalshop.co.il/images/itempics/uploads/media_23062019150926.jpg?rnd=.1262781?rnd=0.8749624067087183',
      authorPhoto:'https://assets.website-files.com/5b9b9d35b759ef07a158b532/5b9ba578899c1843fece25f0_Image-19-03-2018-at-12.05-p-800.png',
    ),
    Recipe(
      title: 'פצצת לוטוס',
      category: 'קינוחים',
      rating: 4.0,
      author: 'אור שפיץ',
      photo: 'http://www.animalshop.co.il/images/itempics/uploads/media_23062019150926.jpg?rnd=.1262781?rnd=0.8749624067087183',
      authorPhoto:'https://assets.website-files.com/5b9b9d35b759ef07a158b532/5b9ba578899c1843fece25f0_Image-19-03-2018-at-12.05-p-800.png',
    ),
  ];
}