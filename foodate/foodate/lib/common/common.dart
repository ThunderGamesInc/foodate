import 'package:flutter/material.dart';

// Colors
const Color kPrimaryColor = Colors.white;
const Color kDarkPrimaryColor = Color(0xffd3b069);
const Color kLightPrimaryColor = Color(0xFFb4a689);
const Color kAccentColor = Colors.pink;
const Color kDividerColor = Colors.pink;
const Color kTitleTextColor = kDarkPrimaryColor;
const Color kBodyTextColor = Colors.black;
const Color kSecondaryTextColor = Color(0xffe6c65e);
