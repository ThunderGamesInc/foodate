import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:foodate/common/common.dart';
import 'package:foodate/navigation/navigation_page.dart';

class FooDateBottomAppbar extends StatelessWidget {
  final int currentIndex;
  final Function onTap;
  final List<NavigationPage> pages;

  FooDateBottomAppbar({this.currentIndex, this.onTap, this.pages});

  List<Widget> buildNavigationItems(List<NavigationPage> pages) =>
      pages
          .map((page) => Icon(page.icon, color: kDarkPrimaryColor))
          .toList();

  @override
  Widget build(BuildContext context) {
    return CurvedNavigationBar(
      backgroundColor: kDarkPrimaryColor,
      onTap: onTap,
      items: buildNavigationItems(pages),
      animationCurve: Curves.easeOutCubic,
    );
  }
}
