import 'package:flutter/material.dart';
import 'package:foodate/common/common.dart';

class FooDateAppBar extends StatefulWidget implements PreferredSizeWidget {
  FooDateAppBar({Key key})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize;

  @override
  _FooDateAppBarState createState() => _FooDateAppBarState();
}

class _FooDateAppBarState extends State<FooDateAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: FittedBox(
              fit: BoxFit.fill,
              child: Text('FooDate',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25, color: kTitleTextColor)),
            ),
          ),
          Flexible(
            child: SizedBox(
              width: 5.0,
            ),
          ),
          Flexible(
            child: FittedBox(
              fit: BoxFit.fill,
              child: Icon(Icons.restaurant, color: kTitleTextColor),
            ),
          ),
        ],
      ),
    );
  }
}
